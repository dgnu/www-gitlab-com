---
layout: handbook-page-toc
title: "Solution Architect Manager Operating Rhythm"
description: "Successful management includes onboarding, coaching, career development and performance management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
